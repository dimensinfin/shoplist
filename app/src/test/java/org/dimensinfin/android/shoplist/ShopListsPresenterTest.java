package org.dimensinfin.android.shoplist;

import android.os.AsyncTask;

import org.dimensinfin.android.shoplist.presenter.ShopListsPresenter;
import org.dimensinfin.android.shoplist.rest.ShopListsClient;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static junit.framework.Assert.fail;
import static org.mockito.Mockito.mock;

public class ShopListsPresenterTest {
    @Mock
    private ShopListsClient mShopListService;
//    @Mock
    private String mShopListView;
    private ShopListsPresenter mShopListsPresenter = null;

    @Before
    public void setupShopListPresenter() {
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test
        mShopListsPresenter = new ShopListsPresenter(mShopListService, mShopListView);
    }

    @Test
    public void loadShopListsFromRepositoryAndLoadIntoView() {
        // Given
        final AsyncTask task = Mockito.mock(AsyncTask.class);
//        fail("Implement in step 6");
//        // Given an initialized NotesPresenter with initialized notes
//        // When loading of Notes is requested
        mShopListsPresenter.loadShopLists(true);
//
//        // Callback is captured and invoked with stubbed notes
//        verify(mNotesRepository).getNotes(mLoadNotesCallbackCaptor.capture());
//        mLoadNotesCallbackCaptor.getValue().onNotesLoaded(NOTES);
//
//        // Then progress indicator is hidden and notes are shown in UI
//        InOrder inOrder = Mockito.inOrder(mNotesView);
//        inOrder.verify(mNotesView).setProgressIndicator(true);
//        inOrder.verify(mNotesView).setProgressIndicator(false);
//        verify(mNotesView).showNotes(NOTES);
    }
}
