package org.dimensinfin.android.shoplist.data;

import android.support.annotation.Nullable;

import com.google.common.base.Objects;

import java.util.UUID;

import lombok.Builder;
import lombok.ToString;

@Builder
@ToString
public class ShopList {
    private final UUID mId;
    private final String mTitle;
    @Nullable
    private final String mDescription;

//    public ShopList(final String title, @Nullable String description) {
//        this.mId = UUID.randomUUID();
//        this.mTitle = title;
//        this.mDescription = description;
//    }

    public boolean isEmpty() {
        return (mTitle == null || "".equals(mTitle)) &&
                (mDescription == null || "".equals(mDescription));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShopList note = (ShopList) o;
        return Objects.equal(mId, note.mId) &&
                Objects.equal(mTitle, note.mTitle) &&
                Objects.equal(mDescription, note.mDescription);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(mId, mTitle, mDescription);
    }
}
