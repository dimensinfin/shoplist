package org.dimensinfin.android.shoplist.presenter;

import android.os.AsyncTask;

import org.dimensinfin.android.shoplist.ShopListsContract;
import org.dimensinfin.android.shoplist.data.ShopList;
import org.dimensinfin.android.shoplist.rest.ShopListsClient;

import java.util.List;

public class ShopListsPresenter implements ShopListsContract.UserActionsListener{
    public ShopListsPresenter(ShopListsClient mShopListRepository, String mShopListView) {
    }

    @Override
    public void loadShopLists(boolean b) {
        AsyncTask.execute(() -> {
            List<ShopList> lists = ShopListsClient.retrieveShopLists();
        });
    }
}
