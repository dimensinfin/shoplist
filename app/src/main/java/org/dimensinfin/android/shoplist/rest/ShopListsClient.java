package org.dimensinfin.android.shoplist.rest;

import org.dimensinfin.android.shoplist.data.ShopList;

import java.util.List;

import feign.Feign;
import feign.Logger;
import feign.RequestLine;
import feign.jackson.JacksonDecoder;

public class ShopListsClient {
    private static final ShopListsBackend shopListsBackend = Feign.builder()
            .decoder(new JacksonDecoder())
            .logger(new Logger.ErrorLogger())
            .logLevel(Logger.Level.BASIC)
            .target(ShopListsBackend.class, "https://shoplist.herokuapp.com");

    public static interface ShopListsBackend {
        @RequestLine("GET /shoplists")
        List<ShopList> retrieveShopLists();
    }

    public static List<ShopList> retrieveShopLists() {
        return shopListsBackend.retrieveShopLists();
    }
}
